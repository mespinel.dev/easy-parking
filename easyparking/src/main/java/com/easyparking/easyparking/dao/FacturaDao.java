
package com.easyparking.easyparking.dao;

import com.easyparking.easyparking.model.Factura;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author Usuario
 */
public interface FacturaDao extends CrudRepository<Factura,Integer> {
    
}
