
package com.easyparking.easyparking.dao;
import com.easyparking.easyparking.model.Usuarios;
import org.springframework.data.repository.CrudRepository;
/**
 *
 * @author Usuario
 */
public interface UsuariosDao extends CrudRepository<Usuarios,Integer>{
    
}
