
package com.easyparking.easyparking.model;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
/**
 *
 * @author Usuario
 */
@Entity
@Table(name="administrador")
public class Administrador implements Serializable {
    @Id
    @Column(name = "idadministrador")
    private Integer idAdministrador;
    
    @Column(name = "user.name")
    private String userName;
    
    @Column(name = "password")
    private String password;

    public Integer getIdAdministrador() {
        return idAdministrador;
    }

    public void setIdAdministrador(Integer idAdministrador) {
        this.idAdministrador = idAdministrador;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
    
}
