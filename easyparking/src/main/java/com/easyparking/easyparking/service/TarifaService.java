
package com.easyparking.easyparking.service;

import com.easyparking.easyparking.model.Tarifa;
import java.util.List;

/**
 *
 * @author ASUS
 */

public interface TarifaService {
    
 public Tarifa save(Tarifa tarifa);
 public void delete(Integer id);
 public Tarifa findById(Integer id);
 public List<Tarifa> findAll();   
    
}
