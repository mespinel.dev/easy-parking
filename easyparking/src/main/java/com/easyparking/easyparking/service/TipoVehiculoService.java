
package com.easyparking.easyparking.service;

import com.easyparking.easyparking.model.TipoVehiculo;
import java.util.List;

/**
 *
 * @author ASUS
 */
public interface TipoVehiculoService {
    
 public TipoVehiculo save(TipoVehiculo tipoVehiculo);
 public void delete(Integer id);
 public TipoVehiculo findById(Integer id);
 public List<TipoVehiculo> findAll();
}
