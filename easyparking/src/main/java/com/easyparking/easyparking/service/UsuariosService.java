package com.easyparking.easyparking.service;

import com.easyparking.easyparking.model.Usuarios;
import java.util.List;


/**
 *
 * @author ASUS
 */
public interface UsuariosService {
    
 public Usuarios save(Usuarios usuarios);
 public void delete(Integer id);
 public Usuarios findById(Integer id);
 public List<Usuarios> findAll();
    
}
