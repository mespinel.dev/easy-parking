
package com.easyparking.easyparking.service;

import com.easyparking.easyparking.model.Vehiculos;
import java.util.List;

/**
 *
 * @author ASUS
 */
public interface VehiculosService {
    
 public Vehiculos save(Vehiculos vehiculos);
 public void delete(Integer id);
 public Vehiculos findById(Integer id);
 public List<Vehiculos> findAll();    
    
}
