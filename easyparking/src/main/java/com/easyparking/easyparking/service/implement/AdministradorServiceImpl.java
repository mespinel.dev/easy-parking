package com.easyparking.easyparking.service.implement;

import com.easyparking.easyparking.dao.AdministradorDao;
import com.easyparking.easyparking.model.Administrador;
import com.easyparking.easyparking.service.AdministradorService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author ASUS
 */
@Service
public class AdministradorServiceImpl implements AdministradorService{
    
    @Autowired
    private AdministradorDao AdministradorDao;

    @Override
    @Transactional(readOnly = false)
    public Administrador save(Administrador administrador) {
        return AdministradorDao.save(administrador);
    }

    @Override
    @Transactional(readOnly = false)
    public void delete(Integer id) {
        AdministradorDao.deleteById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public Administrador findById(Integer id) {
        return AdministradorDao.findById(id).orElse(null);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Administrador> findAll() {
        return (List<Administrador>) AdministradorDao.findAll();
    }

}
