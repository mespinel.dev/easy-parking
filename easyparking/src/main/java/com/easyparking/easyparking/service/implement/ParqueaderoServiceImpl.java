
package com.easyparking.easyparking.service.implement;

import com.easyparking.easyparking.dao.ParqueaderoDao;
import com.easyparking.easyparking.model.Parqueadero;
import com.easyparking.easyparking.service.ParqueaderoService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author ASUS
 */
@Service
public class ParqueaderoServiceImpl implements ParqueaderoService{
    
     @Autowired
    private ParqueaderoDao ParqueaderoDao;

    @Override
    @Transactional(readOnly = false)
    public Parqueadero save(Parqueadero parqueadero) {
        return ParqueaderoDao.save(parqueadero);
    }

    @Override
    @Transactional(readOnly = false)
    public void delete(Integer id) {
        ParqueaderoDao.deleteById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public Parqueadero findById(Integer id) {
        return ParqueaderoDao.findById(id).orElse(null);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Parqueadero> findAll() {
        return (List<Parqueadero>) ParqueaderoDao.findAll();
    }

    
    
}
