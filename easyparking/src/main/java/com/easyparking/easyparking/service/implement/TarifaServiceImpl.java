 
package com.easyparking.easyparking.service.implement;
import com.easyparking.easyparking.dao.TarifaDao;
import com.easyparking.easyparking.model.Tarifa;
import com.easyparking.easyparking.service.TarifaService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
/**
 *
 * @author ASUS
 */
@Service
public class TarifaServiceImpl implements TarifaService{
    
  @Autowired
    private TarifaDao TarifaDao;

    @Override
    @Transactional(readOnly = false)
    public Tarifa save(Tarifa tarifa) {
        return TarifaDao.save(tarifa);
    }

    @Override
    @Transactional(readOnly = false)
    public void delete(Integer id) {
        TarifaDao.deleteById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public Tarifa findById(Integer id) {
        return TarifaDao.findById(id).orElse(null);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Tarifa> findAll() {
        return (List<Tarifa>) TarifaDao.findAll();
    }
  
    
}
