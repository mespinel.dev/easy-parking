
package com.easyparking.easyparking.service.implement;

import com.easyparking.easyparking.dao.TipoVehiculoDao;
import com.easyparking.easyparking.model.TipoVehiculo;
import com.easyparking.easyparking.service.TipoVehiculoService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
/**
 *
 * @author ASUS
 */
@Service
public class TipoVehiculoServiceImpl implements TipoVehiculoService{
    
@Autowired
    private TipoVehiculoDao TipoVehiculoDao;

    @Override
    @Transactional(readOnly = false)
    public TipoVehiculo save(TipoVehiculo tipoVehiculo) {
        return TipoVehiculoDao.save(tipoVehiculo);
    }

    @Override
    @Transactional(readOnly = false)
    public void delete(Integer id) {
        TipoVehiculoDao.deleteById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public TipoVehiculo findById(Integer id) {
        return TipoVehiculoDao.findById(id).orElse(null);
    }

    @Override
    @Transactional(readOnly = true)
    public List<TipoVehiculo> findAll() {
        return (List<TipoVehiculo>) TipoVehiculoDao.findAll();
    }    
    
}
