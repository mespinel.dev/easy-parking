
package com.easyparking.easyparking.service.implement;

import com.easyparking.easyparking.dao.UsuariosDao;
import com.easyparking.easyparking.model.Usuarios;
import com.easyparking.easyparking.service.UsuariosService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author ASUS
 */
@Service
public class UsuariosServiceImpl implements UsuariosService{
    
    @Autowired
    private UsuariosDao UsuariosDao;

    @Override
    @Transactional(readOnly = false)
    public Usuarios save(Usuarios usuarios) {
        return UsuariosDao.save(usuarios);
    }

    @Override
    @Transactional(readOnly = false)
    public void delete(Integer id) {
        UsuariosDao.deleteById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public Usuarios findById(Integer id) {
        return UsuariosDao.findById(id).orElse(null);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Usuarios> findAll() {
        return (List<Usuarios>) UsuariosDao.findAll();
    }

}
